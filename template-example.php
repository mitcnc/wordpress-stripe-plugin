<?php
/**
 * Template Name: My Membership
 *
 * This is the template we use for an alumni club with annual memberships/subscriptions.
 * Users must be logged in so that we can associate the WordPress user to a Stripe customer.
 */

$user_id = get_current_user_id();
if ($user_id > 0) {
    /**
     * Returns the user's active subscription (from Stripe).
     *
     * @param $user_id integer
     *
     * @return \Stripe\Subscription | null
     */
    // phpcs:disable PEAR.NamingConventions.ValidFunctionName.FunctionNoCapital, PEAR.NamingConventions.ValidFunctionName.FunctionNameInvalid
    function get_active_subscription($user_id)
    {
        $stripe_customer = apply_filters('wp_stripe_customer', $user_id);

        // We should never reach this block since the user is logged in, but it's
        // here since we would normally place this method in functions.php so that
        // it can be reused elsewhere, if needed.
        if (is_wp_error($stripe_customer)) {
            return null;
        }

        if ($stripe_customer->subscriptions->count() > 0) {
            return $stripe_customer->subscriptions->data[0];
        }

        return null;
    }
    // phpcs:enable

    $subscription = get_active_subscription($user_id);
    if ($subscription) {
        // Display the customer portal link so the user can manage their subscription.

        $cancel_at_period_end = $subscription->cancel_at_period_end;
        // phpcs:disable WordPress.DateTime.RestrictedFunctions.date_date
        $current_period_end = date('F j, Y', substr($subscription->current_period_end, 0, 10));
        // phpcs:enable
        ob_start();
        ?>
        <p>Your membership will <?php echo esc_html($cancel_at_period_end ? 'end' : 'renew'); ?>
            on <?php echo esc_html($current_period_end); ?>. Click the button below to update your saved payment method,
            <?php echo esc_html($cancel_at_period_end ? 'renew' : 'cancel'); ?> your membership, or view receipts.</p>
        <?php
        echo do_shortcode('[stripe_customer_portal label="Manage membership" class="default-btn red-btn"]');
        $post->post_content = ob_get_clean();
    } else {
        // Display the pricing table so the user can checkout.

        // phpcs:disable WordPress.DateTime.RestrictedFunctions.date_date
        $renew_date = date('F j, Y', strtotime('+1 year'));
        // phpcs:enable
        ob_start();
        ?>
        <p>Select a membership level below. Your membership will start immediately and automatically renew in one
            year on <?php echo esc_html($renew_date); ?>.</p>
        <?php
        echo do_shortcode('[stripe_subscription_checkout checkout_btn_label="Join the Club" checkout_btn_class="default-btn red-btn" price_table_class="table table-striped"]');
        $post->post_content = ob_get_clean();
    }
} else {
    // Not logged in? Let's fix that!
    ob_start();
    ?>
    <h2>You must be logged in to view your membership details</h2>
    <a href="<?php echo esc_url(wp_login_url()); ?>" class="default-btn">
        Login
    </a>
    <?php
    $post->post_content = ob_get_clean();
}

get_header();
?>

<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
        <?php
        while (have_posts()) :
            the_post();
            get_template_part('template-parts/content', 'page');
        endwhile;
        ?>
    </main>
</div>

<?php get_footer(); ?>
