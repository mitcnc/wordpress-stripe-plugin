<?php
/**
 * Plugin Name: Stripe for WordPress
 * Plugin URI: https://gitlab.com/mitcnc/wordpress-stripe-plugin
 * Description: Enable users to make purchases via Stripe.
 * Version: 1.0.0
 * Requires at least: 4.9
 * Requires PHP: 7.2
 * Text Domain: wp-stripe
 * Author: Clinton Blackburn
 * Author URI: https://dev.clintonblackburn.com
 * License: MIT
 **/

namespace WordPress_Stripe;

defined('ABSPATH') || die('No script kiddies please!');


if (is_readable(__DIR__ . '/vendor/autoload.php')) {
    include __DIR__ . '/vendor/autoload.php';
}

use NumberFormatter;
use UnexpectedValueException;
use WP_Error;

class ThePlugin
{
    /**
     * Static property to hold our singleton instance
     */
    public static $instance = null;

    protected const OPTION_NAME = 'wp-stripe';
    protected const OPTION_GROUP = self::OPTION_NAME;
    protected const OPTIONS_PAGE = 'wp-stripe';
    protected const USER_META_PREFIX = 'wp-stripe-';
    protected const USER_META_KEY_CUSTOMER = 'customer-id';

    protected const REST_API_NAMESPACE = 'wp_stripe/v1';
    protected const REST_API_WEBHOOK_ROUTE = '/webhook';
    protected const REST_API_CHECKOUT_ROUTE = '/checkout_session';
    protected const REST_API_SYNC_ROUTE = '/sync_users';

    protected const CHECKOUT_TRANSIENT_PREFIX = 'wp-stripe-checkout--';
    protected const SHORTCODE_CUSTOMER_PORTAL = 'stripe_customer_portal';

    private $stripe_client = null;

    private function __construct()
    {
        add_action('admin_menu', array($this, 'initialize_admin_menu'));
        add_action('admin_init', array($this, 'initialize_settings'));
        add_action('admin_enqueue_scripts', array($this, 'enqueue_admin_scripts'));
        add_action('user_register', array($this, 'on_user_register'), 10, 1);
        add_action('rest_api_init', array($this, 'register_api_routes'));
        add_action('wp_enqueue_scripts', array($this, 'enqueue_scripts'));

        add_filter('wp_stripe_customer', array($this, 'get_or_create_stripe_customer'));

        add_shortcode('stripe_subscription_checkout', array($this, 'render_subscription_checkout'));
        add_shortcode(self::SHORTCODE_CUSTOMER_PORTAL, array($this, 'render_customer_portal'));

        // Add settings link on plugins page
        add_action('plugin_action_links_' . plugin_basename(__FILE__), array($this, 'render_action_links'));

        // Create a hook pointing to the task that will be run by action-scheduler
        add_action('wp_stripe_sync_users', array($this, 'sync_users_with_stripe'));

        // TODO Prevent WooCommerce from modifying the Stripe customer ID with check=false
        //  https://developer.wordpress.org/reference/functions/update_metadata/
        //  https://developer.wordpress.org/reference/functions/delete_metadata/

        add_filter('wc_stripe_update_customer_args', array($this, 'clean_wc_stripe_customer_args'));
        add_filter('get_user_option__stripe_customer_id', array($this, 'get_woocommerce_stripe_customer_id'), 10, 3);
    }

    public static function get_instance()
    {
        if (!self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    private function get_stripe_client()
    {
        if (!$this->stripe_client) {
            $options = $this->get_option();
            $this->stripe_client = new \Stripe\StripeClient(
                array(
                    'api_key' => $options['secret_key'],
                    'stripe_version' => $options['api_version'],
                )
            );
        }

        return $this->stripe_client;
    }

    /**
     * Add a submenu to the Settings menu.
     */
    public function initialize_admin_menu()
    {
        add_options_page(
            __('Configure Stripe', 'wp-stripe'),
            __('Stripe', 'wp-stripe'),
            'manage_options',
            self::OPTIONS_PAGE,
            array($this, 'render_options_page')
        );
    }

    public function register_api_routes()
    {
        register_rest_route(
            self::REST_API_NAMESPACE,
            self::REST_API_WEBHOOK_ROUTE,
            array(
                'methods' => 'POST',
                'callback' => array($this, 'process_stripe_webhook_event'),
                'permission_callback' => '__return_true',
            )
        );
        register_rest_route(
            self::REST_API_NAMESPACE,
            self::REST_API_CHECKOUT_ROUTE,
            array(
                'methods' => 'POST',
                'callback' => array($this, 'create_checkout_session'),
                'permission_callback' => function () {
                    return is_user_logged_in();
                }
            )
        );
        register_rest_route(
            self::REST_API_NAMESPACE,
            self::REST_API_SYNC_ROUTE,
            array(
                'methods' => 'POST',
                'callback' => array($this, 'schedule_sync'),
                'permission_callback' => function () {
                    return current_user_can('edit_plugins');
                }
            )
        );
    }

    /**
     * Gets the option, including defaults if a field is not set.
     *
     * This is especially important for the production setting, which
     * is not set at all if the value was set to false on the settings form.
     *
     * @return array
     */
    private function get_option()
    {
        $option_values = get_option(self::OPTION_NAME);
        $default_values = array(
            'api_version' => '',
            'publishable_key' => '',
            'secret_key' => '',
            'webhook_secret' => '',
            'prices' => '',
            'checkout_cancel_url' => '',
            'checkout_success_url' => '',
            'checkout_allow_promotion_codes' => 0,
            'customer_portal_return_url' => '',
            'woocommerce_reuse_stripe_customers' => 0,
        );
        return shortcode_atts($default_values, $option_values);
    }

    /**
     * Initialize the custom settings page.
     */
    public function initialize_settings()
    {
        register_setting(
            self::OPTION_GROUP,
            self::OPTION_NAME,
            array(
                'sanitize_callback' => array($this, 'validate_options'),
            )
        );
        add_settings_section('api', 'API keys', array($this, 'render_api_section'), self::OPTIONS_PAGE);
        add_settings_section('plans', 'Subscription plans', array($this, 'render_plans_section'), self::OPTIONS_PAGE);
        add_settings_section('checkout', 'Checkout configuration', null, self::OPTIONS_PAGE);
        add_settings_section('customer_portal', 'Customer portal configuration', null, self::OPTIONS_PAGE);
        add_settings_section('users', 'Users', array($this, 'render_users_section'), self::OPTIONS_PAGE);
        add_settings_section('woocommerce', 'WooCommerce', array($this, 'render_woocommerce_section'), self::OPTIONS_PAGE);

        $data = $this->get_option();

        $fields = array(
            array(
                'id' => 'api_version',
                'label' => __('API version', 'wp-stripe'),
                'type' => 'text',
                'section' => 'api',
            ),
            array(
                'id' => 'publishable_key',
                'label' => __('Publishable key', 'wp-stripe'),
                'type' => 'text',
                'section' => 'api',
            ),
            array(
                'id' => 'secret_key',
                'label' => __('Secret key', 'wp-stripe'),
                'type' => 'password',
                'section' => 'api',
            ),
            array(
                'id' => 'webhook_secret',
                'label' => __('Webhook secret', 'wp-stripe'),
                'type' => 'password',
                'section' => 'api',
            ),
            array(
                'id' => 'prices',
                'label' => null,
                'type' => 'hidden',
                'section' => 'plans',
            ),
            array(
                'id' => 'checkout_success_url',
                'label' => __('Success URL', 'wp-stripe'),
                'type' => 'text',
                'section' => 'checkout',
            ),
            array(
                'id' => 'checkout_cancel_url',
                'label' => __('Cancel URL', 'wp-stripe'),
                'type' => 'text',
                'section' => 'checkout',
            ),
            array(
                'id' => 'checkout_allow_promotion_codes',
                'label' => __('Allow promotion codes', 'wp-stripe'),
                'type' => 'checkbox',
                'section' => 'checkout',
            ),
            array(
                'id' => 'customer_portal_return_url',
                'label' => __('Customer portal return URL', 'wp-stripe'),
                'type' => 'text',
                'section' => 'customer_portal',
            ),
            array(
                'id' => 'woocommerce_reuse_stripe_customers',
                'label' => __('Reuse existing Stripe customers', 'wp-stripe'),
                'type' => 'checkbox',
                'section' => 'woocommerce',
            ),
        );
        foreach ($fields as $field) {
            $field_id = $field['id'];
            $args = array_merge(
                $field,
                array(
                    'label_for' => $field_id,
                    'value' => $data[$field_id],
                )
            );
            add_settings_field(
                $field_id,
                $field['label'],
                array($this, 'render_settings_field'),
                self::OPTIONS_PAGE,
                $field['section'],
                $args
            );
        }
    }

    public function enqueue_admin_scripts($hook)
    {
        if ('settings_page_' . self::OPTIONS_PAGE === $hook) {
            wp_enqueue_script('jquery');
            wp_enqueue_script('jquery-ui-core');
            wp_enqueue_script('jquery-ui-sortable');
            wp_enqueue_script('wp_stripe_admin', plugin_dir_url(__FILE__) . '/js/admin.js', array('jquery'), 1);

            $server_vars = array(
                'nonce' => wp_create_nonce('wp_rest'),
                'schedule_url' => get_rest_url(null, self::REST_API_NAMESPACE . self::REST_API_SYNC_ROUTE),
            );
            wp_localize_script('wp_stripe_admin', 'serverVars', $server_vars);
        }
    }

    public function enqueue_scripts($hook)
    {
        wp_enqueue_script('stripejs', 'https://js.stripe.com/v3/', null, 3);
        wp_enqueue_script('jquery');
        wp_enqueue_script('wp_stripe_checkout', plugin_dir_url(__FILE__) . '/js/checkout.js', array('jquery', 'stripejs',), 2);

        $server_vars = array(
            'stripe_publishable_key' => $this->get_publishable_key(),
            'nonce' => wp_create_nonce('wp_rest'),
            'post_url' => get_rest_url(null, $this->get_rest_api_checkout_path()),
        );
        wp_localize_script('wp_stripe_checkout', 'serverVars', $server_vars);
    }

    /**
     * Returns the publishable key for the Stripe API.
     *
     * @return string
     */
    public function get_publishable_key(): string
    {
        return $this->get_option()['publishable_key'];
    }

    public function get_rest_api_checkout_path(): string
    {
        return self::REST_API_NAMESPACE . self::REST_API_CHECKOUT_ROUTE;
    }

    public function validate_options($input)
    {
        $valid = true;

        if (empty($input['publishable_key'])) {
            $valid = false;
            add_settings_error(
                'publishable_key',
                'publishable_key_required',
                __('A publishable key is required to configure Stripe. No changes were saved.', 'wp-stripe')
            );
        }

        if (empty($input['secret_key'])) {
            $valid = false;
            add_settings_error(
                'secret_key',
                'secret_key_required',
                __('A secret secret is required to configure Stripe. No changes were saved.', 'wp-stripe')
            );
        }

        // We return the original data if the passed in data is invalid
        // to ensure we don't overwrite good data with bad.
        return $valid ? $input : $this->get_option();
    }

    private function get_price_ids()
    {
        $price_ids = explode(',', $this->get_option()['prices']);
        $user_id = get_current_user_id();
        $price_ids = apply_filters('wp_stripe_checkout_prices', $price_ids, $user_id);
        return $price_ids;
    }

    private function get_prices()
    {
        $stripe_client = $this->get_stripe_client();
        $prices = $stripe_client->prices->all(
            array(
                'active' => true,
                'type' => 'recurring',
                'expand' => array('data.product')
            )
        )['data'];

        $enabled_price_ids = $this->get_price_ids();

        foreach ($prices as &$price) {
            $price['enabled'] = in_array($price['id'], $enabled_price_ids);
        }

        return $prices;
    }

    private function get_prices_with_discounts()
    {
        $price_ids = $this->get_price_ids();

        $subscription_items = array();
        foreach ($price_ids as $price_id) {
            $subscription_items[] = array(
                'price' => $price_id
            );
        }

        $user_id = get_current_user_id();
        $customer = $this->get_or_create_stripe_customer($user_id);
        $coupon = apply_filters('wp_stripe_checkout_coupon', null, $user_id);

        $params = array(
            'customer' => $customer->id,
            'subscription_items' => $subscription_items,
            'expand' => array('lines.data.price.product'),
        );
        if (!empty($coupon)) {
            $params['coupon'] = $coupon;
        }

        $stripe_client = $this->get_stripe_client();
        $upcoming_invoice = $stripe_client->invoices->upcoming($params);

        $prices = array();
        foreach ($upcoming_invoice->lines->data as $line) {
            $discount_amount = 0;

            foreach ($line['discount_amounts'] as $discount) {
                $discount_amount += $discount['amount'];
            }

            $amount = $line['price']['unit_amount'];
            $prices[] = array(
                'id' => $line['price']['id'],
                'product_name' => $line['price']['product']['name'],
                'currency' => $line['price']['currency'],
                'amount' => $amount,
                'discounted_amount' => $amount - $discount_amount,
            );
        }

        return $prices;
    }

    public function can_make_api_calls()
    {
        return !!$this->get_option()['secret_key'];
    }

    public function render_api_section($arguments)
    {
        printf(
            'Remember to setup your webhook endpoint at <a href="https://dashboard.stripe.com/">Stripe</a>. Make sure to setup live mode or test mode, depending on your API credentials.<br/><br/><code>%1$s</code>',
            esc_url(get_rest_url(null, self::REST_API_NAMESPACE . self::REST_API_WEBHOOK_ROUTE))
        );
    }

    public function render_action_links($links)
    {
        $url = add_query_arg(
            'page',
            self::OPTIONS_PAGE,
            get_admin_url(null, 'options-general.php')
        );

        ob_start();
        ?>
        <a href="<?php echo esc_url($url); ?>">
            <?php esc_html_e('Settings', 'wp-stripe'); ?>
        </a>
        <?php

        $links = array_merge(
            array(ob_get_clean()),
            $links
        );

        return $links;
    }

    public function render_subscription_checkout($attributes)
    {
        $attributes = shortcode_atts(
            array(
                'checkout_btn_label' => __('Checkout', 'wp-stripe'),
                'checkout_btn_class' => '',
                'price_table_class' => '',
            ),
            $attributes,
            self::SHORTCODE_CUSTOMER_PORTAL
        );

        if ($this->can_make_api_calls()) {
            // Get all enabled prices
            $prices = $this->get_prices_with_discounts();
            ob_start();
            ?>
            <style>
                form.stripe-checkout-form .price-row .price-col-amount.has-discount .amount {
                    text-decoration: line-through;
                    margin-right: 8px;
                }
            </style>
            <form class="stripe-checkout-form wp-stripe-rendered">
                <table class="stripe-checkout-price-table <?php echo esc_attr($attributes['price_table_class']); ?>">
                    <?php
                    $fmt = new NumberFormatter('en', NumberFormatter::CURRENCY);

                    foreach ($prices as $price) {
                        $price_id = $price['id'];
                        $product_name = $price['product_name'];
                        $amount = $fmt->formatCurrency($price['amount'] / 100, $price['currency']);
                        $discounted_amount = $fmt->formatCurrency($price['discounted_amount'] / 100, $price['currency']);
                        $has_discount = $amount != $discounted_amount;
                        ?>
                        <tr class="price-row">
                            <td class="price-col-name">
                                <input type="radio" name="price" id="<?php echo esc_attr($price_id); ?>"
                                       value="<?php echo esc_attr($price_id); ?>"/>
                                <label
                                    for="<?php echo esc_attr($price_id); ?>"><?php echo esc_html($product_name); ?></label>
                            </td>
                            <td class="price-col-amount <?php echo $has_discount ? 'has-discount' : ''; ?>">
                                <span class="amount"><?php echo esc_html($amount); ?></span>
                                <?php
                                if ($has_discount) {
                                    ?>
                                    <span class="discounted-amount"><?php echo esc_html($discounted_amount); ?></span>
                                    <?php
                                }
                                ?>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                </table>

                <button class="stripe-checkout-button <?php echo esc_attr($attributes['checkout_btn_class']); ?>"
                        disabled>
                    <?php echo esc_html($attributes['checkout_btn_label']); ?>
                </button>
            </form>
            <?php
            return ob_get_clean();
        } else {
            return '<em>Stripe API key not set</em>';
        }
    }

    public function render_customer_portal($attributes)
    {
        $attributes = shortcode_atts(
            array(
                'label' => __('Manage subscription', 'wp-stripe'),
                'class' => '',
            ),
            $attributes,
            self::SHORTCODE_CUSTOMER_PORTAL
        );
        if (!$this->can_make_api_calls()) {
            return '<em>Stripe API key not set</em>';
        }

        $user_id = get_current_user_id();
        if (0 === $user_id) {
            return '<em>User not logged in</em>';
        }

        $customer = $this->get_or_create_stripe_customer($user_id);
        $options = $this->get_option();
        $return_url = get_home_url(null, $options['customer_portal_return_url']);

        $session = $this->get_stripe_client()->billingPortal->sessions->create(
            array(
                'customer' => $customer->id,
                'return_url' => $return_url,
            )
        );

        ob_start();
        ?>
        <a href="<?php echo esc_url($session->url); ?>" class="<?php echo(esc_attr($attributes['class'])); ?>">
            <?php echo(esc_html($attributes['label'])); ?>
        </a>
        <?php
        return ob_get_clean();
    }

    public function render_users_section($arguments)
    {
        $sync_enabled = $this->can_make_api_calls();
        $url = add_query_arg(
            'page',
            'action-scheduler',
            get_admin_url(null, 'tools.php')
        );
        ?>
        <p>Click the button below to ensure a Stripe customer exists for every user. If the button is disabled, save
            your Stripe credentials first.</p>
        <p>After clicking the button, view the <a href="<?php echo esc_url($url); ?>">scheduled actions page</a> to view
            the task status. In some instances
            you may need to manually start the task.</p>

        <button id="scheduleSync" <?php echo esc_attr($sync_enabled ? '' : 'disabled="disabled"'); ?>>
            Sync users
        </button>

        <?php
    }

    public function render_woocommerce_section($arguments)
    {
        ?>
        <p>Check the box below to use existing Stripe customers (created by WordPress) for WooCommerce orders.
            Otherwise, WooCommerce will associate a new customer with the user at checkout time.</p>
        <?php
    }

    public function render_plans_section($arguments)
    {
        // TODO Validate the API key...or update the rendering to account for cases where the credentials are invalid.
        if ($this->can_make_api_calls()) {
            $prices = $this->get_prices();

            function render_prices_list($id, $prices)
            {
                $fmt = new NumberFormatter('en', NumberFormatter::CURRENCY);
                ?>
                <ul id="<?php echo esc_attr($id); ?>" class="prices-list">
                    <?php
                    foreach ($prices as $price) {
                        // phpcs:disable WordPress.Security.EscapeOutput.OutputNotEscaped
                        printf(
                            '<li id="%1$s">%2$s (%3$s)</li>',
                            $price['id'],
                            $price['product']['name'],
                            $fmt->formatCurrency($price['unit_amount'] / 100, $price['currency'])
                        );
                        // phpcs:enable
                    }
                    ?>
                </ul>
                <?php
            }

            ?>
            <style>
                #enabled-prices-list,
                #disabled-prices-list {
                    list-style-type: none;
                    margin: 0;
                    padding: 0;
                    min-height: 20px;
                    border: 1px solid;
                }

                #enabled-prices-list li,
                #disabled-prices-list li {
                    margin: 3px;
                    padding: 0.4em 1.5em;
                    height: 18px;
                    border: 1px solid;
                    background-color: white;
                    cursor: move;
                }
            </style>

            <p>Select the plans that should be displayed to customers for checkout.</p>

            <h3>Enabled</h3>
            <?php
            render_prices_list(
                'enabled-prices-list',
                array_filter(
                    $prices,
                    function ($price) {
                        return $price['enabled'];
                    }
                )
            );
            ?>

            <h3>Disabled</h3>
            <?php
            render_prices_list(
                'disabled-prices-list',
                array_filter(
                    $prices,
                    function ($price) {
                        return !$price['enabled'];
                    }
                )
            );
        } else {
            echo '<em>Stripe API key not set</em>';
        }
    }


    /**
     * Render an individual field on the settings page.
     *
     * @param $arguments
     */
    public function render_settings_field($arguments)
    {
        $field_id = esc_attr($arguments['id']);
        $value = $arguments['value'];
        $type = esc_attr($arguments['type']);

        // phpcs:disable WordPress.Security.EscapeOutput.OutputNotEscaped
        //  Value is the only user-provided field, and it is sanitized above.
        switch ($arguments['type']) {
            case 'hidden':
            case 'password':
            case 'text':
                printf(
                    '<input name="%1$s[%2$s]" id="%2$s" type="%3$s" value="%4$s" required />',
                    esc_attr(self::OPTION_NAME),
                    $field_id,
                    $type,
                    esc_attr($value)
                );
                break;
            case 'checkbox':
                printf(
                    '<input name="%1$s[%2$s]" id="%2$s" type="%3$s" value="1" %4$s />',
                    esc_attr(self::OPTION_NAME),
                    $field_id,
                    $type,
                    checked(1, $value, false)
                );
                break;
            default:
                die("Encountered unexpected field type: {$arguments['type']}");
        }
        // phpcs:enable
    }

    /**
     * Render the options page where users configure the OAuth 2.0 client.
     */
    public function render_options_page()
    {
        ?>
        <div class="wrap">
            <h2><?php echo esc_html(get_admin_page_title()); ?></h2>
            <form action="options.php" method="post" autocomplete="off">
                <?php
                settings_fields(self::OPTION_GROUP);
                do_settings_sections(self::OPTIONS_PAGE);
                submit_button();
                ?>
            </form>
        </div>
        <?php
    }

    /**
     * Find a Stripe customer via email.
     *
     * @param $email string
     *
     * @return \Stripe\Customer | WP_Error | null
     */
    public function get_stripe_customer_by_email($email)
    {
        $stripe_client = $this->get_stripe_client();
        try {
            $customers = $stripe_client->customers->all(
                array(
                    'email' => $email,
                    'expand' => array('data.subscriptions'),
                )
            );
            $customers = array_filter(
                $customers->data,
                function ($customer) {
                    return !$customer->isDeleted();
                }
            );

            $customer_count = count($customers);
            if (1 == $customer_count) {
                return $customers[0];
            } elseif ($customer_count > 1) {
                $customer = $customers[0];
                error_log("Multiple Stripe customers found with email. Using the oldest one. email={$email} customer={$customer->id}");
                return $customer;
            }
        } catch (\Stripe\Exception\InvalidRequestException $e) {
            error_log("Failed to find Stripe customer by email: {$e}");
        }

        return null;
    }

    /**
     * Get the Stripe customer associated with the user, or create a new customer on Stripe if one does not exist.
     *
     * @param $user_id integer
     *
     * @return \Stripe\Customer | WP_Error
     */
    public function get_or_create_stripe_customer(int $user_id)
    {
        if (0 === $user_id) {
            return new WP_Error('not_logged_in', 'Login is required to get or create a Stripe customer');
        }

        $customer_token = get_user_meta($user_id, self::USER_META_PREFIX . self::USER_META_KEY_CUSTOMER, true);
        $stripe_client = $this->get_stripe_client();

        $user = get_userdata($user_id);
        $customer_metadata = array('wordpress_user_id' => $user_id);
        $stripe_user_data = array(
            'expand' => array('subscriptions'),
            'name' => trim($user->user_firstname . ' ' . $user->last_name) ?? null,
            'email' => $user->user_email,
            'metadata' => $customer_metadata,
        );

        $customer = null;

        if ($customer_token) {
            try {
                $customer = $stripe_client->customers->retrieve(
                    $customer_token,
                    array(
                        'expand' => array('subscriptions.data.plan.product'),
                    )
                );

                // If the customer is deleted, let's go ahead and make a new one.
                // We want to optimize for users taking actions (e.g., checking out
                // and managing subscriptions).
                if ($customer->isDeleted()) {
                    $customer = null;
                }
            } catch (\Stripe\Exception\InvalidRequestException $e) {
                if ($e->getHttpStatus() != 404) {
                    throw $e;
                }

                // The data in WordPress is invalid. The customer no longer exists.
                // No worries. We'll just make a new one!
            }
        }

        if (is_null($customer)) {
            // Try searching via email in case the customer was created outside of WordPress.
            $customer = $this->get_stripe_customer_by_email($user->user_email);
        }

        // Sync an existing customer. If we make a new customer, the correct data will be written
        // during the creation API call.
        if (!is_null($customer)) {
            $customer = $this->sync_customer_and_user($customer, $stripe_user_data);
        }

        if (is_null($customer)) {
            // Finally...create a new customer!
            $customer = $stripe_client->customers->create($stripe_user_data);
        }

        // Keep the metadata updated whether we create a new customer, or associate with an existing one.
        update_user_meta($user_id, self::USER_META_PREFIX . self::USER_META_KEY_CUSTOMER, $customer->id);
        // TODO Add action hook to set WooCommerce metadata

        return $customer;
    }

    public function on_user_register($user_id)
    {
        if ($this->can_make_api_calls()) {
            $this->get_or_create_stripe_customer($user_id);
        }
    }

    public function process_stripe_webhook_event($request)
    {
        $options = $this->get_option();
        $secret_key = $options['secret_key'];
        $webhook_secret = $options['webhook_secret'];

        if (!($secret_key && $webhook_secret)) {
            return new WP_Error('no_api_key', 'Stripe API key not set');
        }

        if (!isset($_SERVER['HTTP_STRIPE_SIGNATURE'])) {
            return new WP_Error('signature_missing', 'Signature missing', array('status' => 400));
        }

        \Stripe\Stripe::setApiKey($secret_key);

        $payload = @file_get_contents('php://input');
        $sig_header = sanitize_text_field(wp_unslash($_SERVER['HTTP_STRIPE_SIGNATURE']));
        $event = null;

        try {
            $event = \Stripe\Webhook::constructEvent($payload, $sig_header, $webhook_secret);
        } catch (UnexpectedValueException $e) {
            return new WP_Error('invalid_payload', 'Invalid payload', array('status' => 400));
        } catch (\Stripe\Exception\SignatureVerificationException $e) {
            return new WP_Error('invalid_signature', 'Invalid signature', array('status' => 400));
        }

        do_action('wp_stripe_process_webhook_event', $event);
        return array('success' => true);
    }

    public function schedule_sync($request)
    {
        as_enqueue_async_action('wp_stripe_sync_users');
    }

    public function create_checkout_session($request)
    {
        try {
            $user_id = get_current_user_id();
            $line_items = $request['line_items'];
            $session = $this->create_stripe_checkout_session($user_id, $line_items);
            return array('id' => $session->id);
        } catch (\Stripe\Exception\ApiErrorException $e) {
            return new WP_Error('stripe_api_error', $e->getMessage());
        }
    }

    private function create_stripe_checkout_session($user_id, $line_items)
    {
        $customer = $this->get_or_create_stripe_customer($user_id);
        $options = $this->get_option();
        $success_url = get_home_url(null, $options['checkout_success_url'] . '?session_id={CHECKOUT_SESSION_ID}');
        $cancel_url = get_home_url(null, $options['checkout_cancel_url']);
        $session_params = array(
            'success_url' => $success_url,
            'cancel_url' => $cancel_url,
            'payment_method_types' => array('card'),
            'customer' => $customer->id,
            'line_items' => $line_items,
            'mode' => 'subscription',
        );
        $coupon = apply_filters('wp_stripe_checkout_coupon', null, $user_id);
        if (empty($coupon)) {
            if (boolval($options['checkout_allow_promotion_codes'])) {
                $session_params['allow_promotion_codes'] = true;
            }
        } else {
            $session_params['discounts'] = array(
                array('coupon' => $coupon),
            );
        }
        $session = $this->get_stripe_client()->checkout->sessions->create($session_params);

        // Store the Checkout session ID so we can look it up after the session is completed.
        // Sessions expire after 24 hours. We store for 23 hours to play it safe.
        set_transient(self::CHECKOUT_TRANSIENT_PREFIX . $user_id, $session->id, 23 * 60 * 60);

        return $session;
    }

    /**
     * Create a Stripe customer for every user.
     */
    public function sync_users_with_stripe()
    {
        if (!$this->can_make_api_calls()) {
            error_log('Unable to sync users with Stripe. API credentials not setup.');
            return;
        }

        $users = get_users();

        foreach ($users as $user) {
            $this->get_or_create_stripe_customer($user->ID);
        }
    }


    /**
     * Update the Stripe customer with the latest user data.
     *
     * @param \Stripe\Customer $customer
     * @param array $stripe_user_data
     * @return \Stripe\Customer
     */
    protected function sync_customer_and_user(\Stripe\Customer $customer, array $stripe_user_data): \Stripe\Customer
    {
        $stripe_client = $this->get_stripe_client();

        try {
            // Ensure we keep WordPress and Stripe synced
            $stripe_user_data['metadata'] = array_merge(
                $customer->metadata->toArray(),
                $stripe_user_data['metadata']
            );
            $customer = $stripe_client->customers->update($customer->id, $stripe_user_data);
        } catch (\Stripe\Exception\ApiErrorException $e) {
            $this->log_sentry_exception($e);
        }

        return $customer;
    }

    private function log_sentry_exception($e)
    {
        if (function_exists('wp_sentry_safe')) {
            wp_sentry_safe(
                function (\Sentry\State\HubInterface $client) use ($e) {
                    $client->captureException($e);
                }
            );
        }
    }

    /**
     * Modify the arguments sent to the Stripe customer API.
     *
     * @param array $args Arguments used to create or update Stripe customer.
     * @return array
     */
    public function clean_wc_stripe_customer_args(array $args): array
    {
        if (!boolval($this->get_option()['woocommerce_reuse_stripe_customers'])) {
            return $args;
        }

        // Let this plugin define name and email, instead of WooCommerce.
        unset($args['name']);
        unset($args['email']);

        return $args;
    }

    public function get_woocommerce_stripe_customer_id($result, $option, $user): string
    {
        if (is_null($user) || !boolval($this->get_option()['woocommerce_reuse_stripe_customers'])) {
            return $result;
        }

        try {
            $customer = $this->get_or_create_stripe_customer($user->ID);

            if (is_wp_error($customer)) {
                return $result;
            }

            return $customer->id;
        } catch (Stripe\Exception\InvalidRequestException $e) {
            return $result;
        }
    }
}

ThePlugin::get_instance();
