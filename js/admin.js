jQuery(document).ready(function ($) {
    const nonce = serverVars.nonce;
    const scheduleUrl = serverVars.schedule_url;
    const $scheduleBtn = $('button#scheduleSync');

    const $pricesList = $('#enabled-prices-list');
    const $pricesField = $('#prices');

    $('#enabled-prices-list, #disabled-prices-list').sortable({
        connectWith: '.prices-list',
        cursor: 'move',
        update: function (event, ui) {
            const prices = $pricesList.sortable('toArray');
            $pricesField.val(prices);
        }
    }).disableSelection();

    $scheduleBtn.click(function (e) {
        $(e.target).prop('disabled', true);

        $.ajax({
            url: scheduleUrl,
            method: 'post',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('X-WP-Nonce', nonce);
            },
            error: function (jqXHR) {
                console.log(jqXHR);
                alert('Queuing failed!');
                $(e.target).prop('disabled', false);
            },
            success: function (data) {
                console.log(data);
                alert('Sync queued.');
                $(e.target).prop('disabled', false);
            }
        });

        e.stopPropagation();
        return false;
    });
});
