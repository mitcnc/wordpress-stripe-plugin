jQuery(document).ready(function ($) {
    const nonce = serverVars.nonce;
    const $checkoutBtn = $('.stripe-checkout-form.wp-stripe-rendered button');

    $(".stripe-checkout-form.wp-stripe-rendered input[name='price']").click(function (e) {
        $checkoutBtn.prop('disabled', false);
    });
    $checkoutBtn.on('click.wp-stripe', function (e) {
        const stripe = Stripe(serverVars.stripe_publishable_key);
        const price = $(".stripe-checkout-form.wp-stripe-rendered input[name='price']:checked").val();
        const lineItems = [
            {
                price: price,
                quantity: 1,
            },
        ];
        $(e.target).prop('disabled', true);

        $.ajax({
            url: serverVars.post_url,
            method: 'post',
            data: {
                line_items: lineItems,
            },
            beforeSend: function (xhr) {
                xhr.setRequestHeader('X-WP-Nonce', nonce);
            },
            error: function (jqXHR) {
                console.log(jqXHR);
                $(e.target).prop('disabled', false);
            },
            success: function (data) {
                console.log(data);
                stripe.redirectToCheckout({sessionId: data.id});
            }
        });

        e.stopPropagation();
        return false;
    });
});
