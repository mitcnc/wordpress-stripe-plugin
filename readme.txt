=== Stripe for WordPress ===
Contributors: ccb621
Donate link: https://blackburnfoundation.org/donate/
Tags: payment, Stripe, checkout, subscription
Requires at least: 4.9
Tested up to: 4.9
Requires PHP: 8.x
Stable tag: trunk
License: MIT

Enable users to make purchases via Stripe.

== Description ==

This plugin enables WordPress installations to use Stripe to make purchases.
The plugin makes use of Stripe Checkout [0] to facilitate purchases without the
need to create a custom UI. Stripe's customer portal [1] is used to enable
users to manage their subscriptions and saved payment methods.

[0] https://stripe.com/docs/payments/checkout
[1] https://stripe.com/docs/billing/subscriptions/integrating-customer-portal

== Installation ==

1. Install the plugin via Composer. See https://www.smashingmagazine.com/2019/03/composer-wordpress/
   to learn more about installing WordPress plugins via Composer.
2. Activate the plugin as you would any other plugin.
3. Configure the Stripe credentials at `Settings > Stripe`.
4. Setup a webhook endpoint at Stripe [2] to listen for the following events:
    - customer.subscription.created
    - customer.subscription.deleted
    - customer.subscription.updated

[2] https://stripe.com/docs/webhooks

== Sync users ==

A Stripe customer is created (if one does not exist) whenever a user is created on the site or a checkout/customer
portal session is created. If you need to sync all users, there is a background task powered by action-scheduler [3].
Once this plugin is installed, click the "Sync users" button on the options page to start the sync.

[3] https://actionscheduler.org/

== Shortcodes ==

Shortcodes are used to create links to Stripe Checkout and the customer portal. See `template-example.php`
for an example of the shortcodes included in this plugin.

Checkout sessions can be created using the `stripe_subscription_checkout` shortcode. This will create a
table listing the available subscription prices/plans, and button that begins the checkout process.

This shortcode has the following attributes:

- checkout_btn_class: The CSS class set on the checkout button.
- checkout_btn_label: The text displayed on the checkout button. This defaults to "Checkout".
- price_table_class: The CSS class set on the price/plan table.

The `stripe_customer_portal` shortcode can be used to generate link to the customer portal.
If the user is not logged in (e.g, user ID is 0), the text "User not logged in" is displayed.
We recommend you only render this shortcode on pages that require login.

If no API keys are configured, the text "Stripe API key not set" is displayed. We recommend you always
configure API keys

The link can be customized by passing the following attributes to the shortcode:

- class: CSS class set on the <a> tag
- label: The text displayed. This defaults to "Manage subscription".

== Actions ==

Need the Stripe customer associated with a user? Use the `wp_stripe_customer` action. The data will
be retrieved from the Stripe API and returned as an associative array matching the schema of the Stripe API [4].

If no customer exists for this user, one will be created.

An error will be returned if the user ID is 0 (e.g., the user is not logged in).

    do_action( 'wp_stripe_customer', $user->ID );

[4] https://stripe.com/docs/api/customers


The plugin will return a successful response for all webhook notification/events, without acting on them. This ensures
Stripe does not continue sending notifications that fail to be processed. This can be overridden by adding a callback
for the `wp_stripe_process_webhook_event` action.

Your callback will receive a single argument containing the event data. Your callback's return value will be ignored,
and the plugin will respond as if the processing succeeded. Raising an error will halt processing, resulting in an error
response. This will cause Stripe to resend the event later.

== Custom prices ==

The `wp_stripe_checkout_prices` filter can be used to modify the Stripe prices presented to the user. This may be useful
if you wish to conditionally enable certain prices for a given user. The callback is passed the array of price IDs that
have been activated on the plugin settings page, and the ID of the current user.

== Coupons ==

The `wp_stripe_checkout_coupon` filter can be used to specify a coupon to be applied to the Checkout session.
The callback is passed the default coupon ID, `null`, and the ID of the current user. Empty [5] return values
will be ignored, so the callback should return `null` if no discount should be applied.

If a discount is active, the checkout form will render both the original and discounted amounts for the product.
By default, the form includes CSS styles to render the original amount with a line through the text and the discounted
amount rendered beside it (e.g., cross out pricing). You can override these styles with the following CSS selectors:

    form.stripe-checkout-form .price-row .price-col-amount.has-discount .amount
    form.stripe-checkout-form .price-row .price-col-amount.has-discount .discounted-amount

[5] https://www.php.net/manual/en/function.empty.php

== Development ==

Always use test mode credentials during development.

Need to debug an error? Add the block of code below to `wordpress-stripe.php` to display errors in the browser.

    ini_set('display_errors', 1);
    error_reporting(E_ALL);

Need to use ngrok? See https://ngrok.com/docs#wordpress.
