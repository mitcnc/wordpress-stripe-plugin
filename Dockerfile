FROM wordpress:6.0-php8.1-apache

RUN apt-get update && apt-get install -y git make vim wget zip  && rm -rf /var/lib/apt/lists/*
RUN wget https://raw.githubusercontent.com/composer/getcomposer.org/76a7060ccb93902cd7576b67264ad91c8a2700e2/web/installer -O - -q | php --
RUN mv composer.phar /usr/local/bin/composer

WORKDIR /var/www/html/wp-content/plugins/wordpress-stripe
